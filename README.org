#+TITLE: README
#+AUTHOR: Bryan Rinders
#+DATE: <2024-04-08>
#+OPTIONS: ^:{} toc:nil num:t
#+PROPERTY: header-args :exports code :eval no-export

* Bryan's DWM -- v6.4
This build of dwm is patched to be used with [[https://gitlab.com/bryos/dwmblocks][dwmblocks]], a statusbar.
And uses a gruvbox like color theme.

* Patches
The patches applied in this build are all located in the =patches=
directory.

#+begin_src sh :results output list :exports results
  ls patches
#+end_src

#+RESULTS:
: - dwm-attachbottom-6.3.diff
: - dwm-dwmblocks-6.4.diff
: - dwm-movestack-20211115-a786211.diff

* Keybindings
RTFC, see [[file:config.def.h]]. One thing to note is that it is designed
with the [[https://colemak.com/][colemak]] keyboard layout in mind.

* Installation
For installation instructions and other technical stuff see the
original [[file:README][README]].
